a. Qual foi o nível de dificuldade da implementação da questão do bloco A?
[ ] Muito Fácil
[ ] Fácil
[ ] Médio
[ x ] Difícil
[ ] Muito Difícil

b. Qual foi o nível de dificuldade para encontrar os erros do programa da questão 7?
[ x ] Muito Fácil
[ ] Fácil
[ ] Médio
[ ] Difícil
[ ] Muito Difícil


OBS.: Quando foi concedido o tempo a mais, tentei fazer a questão 2, porém, ainda ficou faltando implementar o javaScript nela.