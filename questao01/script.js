var notasLancadas = []
const btnSalvarNota = document.getElementById("idBtnNovaNota")
const btnRelatorio = document.getElementById("idBtnRelatorio")
const inputNota = document.getElementById("idNota")
const out = document.getElementById("idOutput")


btnSalvarNota.addEventListener("click",function(){
    let nota = Number(inputNota.value)
    notasLancadas.push(nota)
    inputNota.value = ""
})

btnRelatorio.addEventListener("click", function(){
    if(notasLancadas.length != 0){
        let media = calcularMedia()
        let aluno = document.getElementById("idNomeAluno").value
        let conceito
        inputNota.value = ""
    
        if (media >= 8.5){
            conceito = "A"
        }else if(media >= 7){
            conceito = "B"
        }else if(media >= 5){
            conceito = "C"
        }else{
            conceito = "D"
        }
    
        exibirResultado(media, aluno, conceito)
    }else{
        out.value = "Nenuma nota informada para este aluno"
    }

})

function calcularMedia(){
    excluirMenor()
    excluirMaior()
    let soma = 0
    for(let i=0; i<notasLancadas.length; i++){
        soma += notasLancadas[i]
    }
    return soma/notasLancadas.length
}

function excluirMaior(){
    let imaior = 0
    for(let i=0; i<notasLancadas.length; i++){
        if(notasLancadas[i] > notasLancadas[imaior]){
            imaior=i
        }
    }
    notasLancadas.splice(imaior,1)
}

function excluirMenor(){
    let imenor = 0
    for(let i=0; i<notasLancadas.length; i++){
        if(notasLancadas[i] > notasLancadas[imenor]){
            imaior=i
        }
    }
    notasLancadas.splice(imenor,1)
}

function exibirResultado(media, aluno, conceito){
    out.innerHTML = "<b>ALUNO: </b>"+aluno+"<br><b>MEDIA FINAL: </b>"+media+"<br> <b>CONCEITO: </b>"+conceito
}
